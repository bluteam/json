CREATE TABLE books (
  title VARCHAR(50),
  author VARCHAR(50),
  category VARCHAR(20),
  year YEAR(4), 
  price DECIMAL(6, 2)
);

INSERT INTO books VALUES('Freakonomics', 'Steven Levitt', 'finance', '2006', '18.00');

INSERT INTO books VALUES('Harry Potter and the Sorceror''s Stone', 'J.K. Rowling', 'children', '1998', '19.99');

INSERT INTO books VALUES('Breakfast for Dinner', 'Amanda Camp', 'cooking', '2009', '22.00');

INSERT INTO books VALUES('Web Programming Step-by-Step', 'Marty Stepp, Jessica Miller', 'computers', '2009', '999.99');

INSERT INTO books VALUES('Building Java Programs', 'Stuart Reges, Marty Stepp', 'computers', '2007', '90.00');

